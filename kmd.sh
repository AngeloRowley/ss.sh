sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install build-essential pkg-config libc6-dev m4 g++-multilib autoconf libtool ncurses-dev unzip git zlib1g-dev wget curl bsdmainutils automake cmake clang ntp ntpdate nano -y
wget https://github.com/KomodoPlatform/komodo/releases/download/0.7.1/komodo_0.7.1_linux.zip
unzip komodo_0.7.1_linux.zip
tar -xvf linux64.tar.gz
cd linux64
wget https://gitlab.com/AngeloRowley/ss.sh/-/raw/main/komodo.conf
cd ~
cd komodo/src
./komodod &
./komodo-cli -ac_name=CLC setgenerate true $(nproc)
